#ifndef _strainGauge
#define _strainGauge

/*
 * Loop until strain gauge is successfully calibrated to target voltage.
 * Light LED on ERROR_PIN on failure, clear it once successful.
 * Call attemptCalibration() instead if you want one attempt instead of a loop.
 */
void strainGaugeCalibrate(int target);
/*
 * Single attempt to calibrate pot to target (0-1023) voltage.
 * Return true if successful, false if not.
 */
bool attemptCalibration(int target);
/*
 * Read voltage level specified number of times, then return average (0-1023).
 */
int readGauge(int avgTimes);
/*
 * Write resistance value (0-1023) to digital pot.
 */
void writeValue(int resistance);
/*
 * Toggle pin high and low specified number of times.
 * This doesn't exactly belong here, but it's close enough.
 */
void blinkLed(int pin, int times);

#endif
