#ifndef _ring_queue
#define _ring_queue
#include <stdint.h>
#include "config.h"

// Note: PACKET_COUNT is defined in config.h
const uint16_t SAMPLE_COUNT = 24;
const uint16_t QUEUE_SIZE = PACKET_COUNT * SAMPLE_COUNT;
const uint16_t SAMPLE_SIZE = 10;

struct Packet {
    volatile uint8_t deviceId;
    volatile uint8_t packetId;
    volatile uint8_t data[30];
};

struct Queue {
    volatile Packet pkts[PACKET_COUNT];
    volatile uint16_t head;
    volatile uint16_t tail;
};

/*
 * Number of elements (samples) stored in queue.
 */
uint_fast16_t queueElements(Queue *queue);

/*
 * Number of elements in smallest queue.
 */
uint_fast16_t queueLeastElements(Queue *queues, int numQueues);

/*
 * Append sample value to end of queue, assuming there is room. Fails silently
 * if not.
 */
bool queueAppend(Queue *queue, const uint_fast16_t value);

/*
 * Pop (remove and return) sample from queue.
 */
uint_fast16_t queuePop(Queue *queue);
#endif
