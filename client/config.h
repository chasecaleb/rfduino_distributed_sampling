#ifndef _config
#define _config

#define DEVICE_ID 0
#define READ_HZ 500

#define PACKET_COUNT 150

#define READY_PIN 3  // Light up LED when finished with setup.
#define ERROR_PIN 4  // Light up LED in case of wireless dropouts.

#endif
