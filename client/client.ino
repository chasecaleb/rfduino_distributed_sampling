#include <string.h>
#include <RFduinoGZLL.h>
#include <Wire.h>  // Required *here* for strainGauge.h/cpp
#include "shared.h"
#include "ring_queue.h"
#include "strainGauge.h"
#include "config.h"

Queue samples = {{}, 0, 0};
volatile unsigned long startTime = 0;
volatile uint8_t packetId = 0;
volatile char oldPacket[32];
volatile char sendPacket[32];
volatile bool sendReady = false;

void setup() {
    pinMode(READY_PIN, OUTPUT);
    pinMode(ERROR_PIN, OUTPUT);

    blinkLed(READY_PIN, 5);
    strainGaugeCalibrate(500);
    blinkLed(READY_PIN, 10);

    setupGzll();
    while (startTime == 0 || micros() < startTime)
        ;
    setupTimer(getSample);
    digitalWrite(READY_PIN, HIGH);  // Indicate that sampling is starting.
}

/*
 * Setup GZLL wireless, with each slave device on a separate channel. This
 * way the master can switch between channels to communicate with specific
 * devices, without them all interfering with each other.
 */
void setupGzll() {
    RFduinoGZLL.begin(HOST);  // This does basic setup/boilerplate for us.
    switchChannel(DEVICE_ID);
}

void loop() {
    delay(1000);
}

void getSample() {
    /* static unsigned int i = 100; */
    /* i++; */
    /* if (i > 999) i = 100; */
    /* noInterrupts(); */
    /* const bool success = queueAppend(&samples, i); */
    /* interrupts(); */
    const bool success = queueAppend(&samples, analogRead(2));

    // Light up error LED if queue is full.
    if (!success) digitalWrite(ERROR_PIN, HIGH);
    // Reset hardware timer.
    NRF_TIMER1->EVENTS_COMPARE[0] = 0;
}

void RFduinoGZLL_onReceive(device_t device, int rssi, char *data, int len) {
    if (data[0] != DEVICE_ID) return;  // Packet meant for another device.

    if (startTime == 0) {  // Setup-related messages.
        handleSetup(data, len);
    } else if (len == 2) {  // Make sure request meant for this device.
        handleSampleRequest(data[1]);
    }
}


void handleSetup(char *data, int len) {
    char ack[2];
    ack[0] = DEVICE_ID;

    if (len == 6 && data[1] == 'c') {  // Start/countdown time.
        unsigned long countdown;
        memcpy(&countdown, &data[2], sizeof(countdown));
        startTime = micros() + countdown;
        ack[1] = 't';
    } else if (len == 2 && data[1] == 'p') {
        ack[1] = 'p';
    }

    // Tell host that time or ping was received.
    RFduinoGZLL.sendToDevice(DEVICE0, ack, sizeof(ack));
}

/*
 * Called by onRecieve to send analog samples to host.
 */
void handleSampleRequest(uint8_t requestedId) {
    if (requestedId >= packetId) {  // Master requested a new packet.
        noInterrupts();
        const uint_fast16_t currentSamples = queueElements(&samples);
        if (currentSamples / SAMPLE_COUNT >= 1) {
            createPacket();
            interrupts();
            // FIXME: this shouldn't be necessary to stop race condition...
            sendPacket[1] = packetId;
            RFduinoGZLL.sendToDevice(DEVICE0, (char *)sendPacket, 32);
            packetId++;
        } else {
            // Master received our last packet (since it is requesting a new
            // packet id), but there are not enough samples to send yet.
            sendReady = false;
            interrupts();
        }
    } else if (sendReady && (requestedId + 1) == packetId) {
        // Master requesting the same packet again -- either it didn't get
        // it the first time or this is a delayed message.
        RFduinoGZLL.sendToDevice(DEVICE0, (char *)sendPacket, 32);
    }
}

/*
 * Update sendPacket global with DEVICE_ID, packetId, and analog samples
 * from queue so that it can be sent to the host.
 */
void createPacket() {
    const unsigned int position = samples.tail / SAMPLE_COUNT;
    samples.pkts[position].deviceId = DEVICE_ID;
    samples.pkts[position].packetId = packetId;
    memcpy((void *)&sendPacket, (void *)&samples.pkts[position], sizeof(sendPacket));
    samples.tail = (samples.tail + SAMPLE_COUNT) % QUEUE_SIZE;

    sendReady = true;
}
