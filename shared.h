#ifndef _shared
#define _shared

/******************************************************
 * Miscellaneous code shared between host and client. *
 ******************************************************/
// First wireless channel to be used.
// Adjusting this between 0 - 100 may help with interence.
#define CHANNEL_OFFSET 32

/*
 * Low-level functions that are normally wrapped by the RFduinoGZLL library.
 * REFERENCES:
 * https://devzone.nordicsemi.com/documentation/nrf51/4.3.0/html/group__gzll__02__user__guide.html
 * https://devzone.nordicsemi.com/documentation/nrf51/4.3.0/html/group__gzll__02__api.html
 */
extern "C" {
    bool nrf_gzll_set_channel_table(uint8_t *channel_table, uint32_t size);
    void nrf_gzll_disable(void);
    bool nrf_gzll_enable(void);
    bool nrf_gzll_is_enabled(void);
}

void setupTimer(callback_t callback);
void switchChannel(uint8_t channel);

#endif
