#ifndef _config
#define _config

#define CLIENTS 5
#define READ_HZ 500
#define LOCAL_SAMPLING false  // Whether or not to take samples on host.

// TOTAL_DEVICES is either equal to clients or clients + 1 host. Don't touch.
#if LOCAL_SAMPLING
#define TOTAL_DEVICES (CLIENTS + 1)
#else
#define TOTAL_DEVICES CLIENTS
#endif

// Output for LabView program if true, or human-readable display if not.
// WARNING: setting to false may slow down output too much.
#define LABVIEW_OUTPUT true

#if LABVIEW_OUTPUT
#define SERIAL_SPEED 250000
#else
#define SERIAL_SPEED 115200
#endif

// Use half (4kB out of 8kB) of the ram for packets.
// #define PACKET_COUNT 4096/(32*(CLIENTS+1))
#define PACKET_COUNT 25

#define READY_PIN 3  // Light up LED when finished with setup.
#define ERROR_PIN 4  // Light up LED in case of wireless dropouts.

#endif
