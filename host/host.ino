#include <string.h>
#include <cstdio>
#include <RFduinoGZLL.h>
#include <Wire.h>  // Required *here* for strainGauge.h/cpp
#include "shared.h"
#include "ring_queue.h"
#include "strainGauge.h"
#include "config.h"

Queue samples[TOTAL_DEVICES] = {};

volatile bool pingAcknowledged;
volatile bool timeAcknowledged;
volatile uint8_t channel = CLIENTS - 1;  // Current channel GZLL channel.
volatile uint8_t packetId[CLIENTS] = {0};  // Tracks which client samples have been received.

/*
 * Call calibration code, setup serial/LEDs/wireless, wait until all clients
 * are online, send sampling start (countdown) time to all clients, and then
 * wait until time to start sampling/main loop().
 * Also lights up READY_PIN when countdown is up.
 */
void setup() {
    Serial.begin(SERIAL_SPEED);
    pinMode(READY_PIN, OUTPUT);
    pinMode(ERROR_PIN, OUTPUT);

    blinkLed(READY_PIN, 5);  // Power-on indicator.
    // Calibrate digital pot for strain gauge readings if used.
    if (LOCAL_SAMPLING) {
        strainGaugeCalibrate(500);
        blinkLed(READY_PIN, 10);  // Calibration success indicator.
    }

    RFduinoGZLL.begin(DEVICE0);

    // Ping each device until online
    for (int i = 0; i < CLIENTS; i++) {
        nextChannel();
        pingAcknowledged = false;
        while (!pingAcknowledged) {
            char ping[] = {channel, 'p'};
            RFduinoGZLL.sendToHost(ping, sizeof(ping));
        }
    }
    if (!LABVIEW_OUTPUT) Serial.println("devices online");

    // Broadcasts countdown to all clients to synchronize sampling.
    unsigned long startTime = micros() + 3000000;
    for (int i = 0; i < CLIENTS; i++) {
        nextChannel();
        char packet[6];
        packet[0] = channel;
        packet[1] = 'c';

        timeAcknowledged = false;
        while (!timeAcknowledged) {
            // Subtracting 10,000 accounts for communication latency... roughly.
            const unsigned long countdown = startTime - micros() - 10000;
            memcpy(&packet[2], (char *)&countdown, sizeof(countdown));
            RFduinoGZLL.sendToHost(packet, sizeof(packet));
            delay(25);
        }
    }
    if (!LABVIEW_OUTPUT) Serial.println("start sent");

    nextChannel();  // Switch one more time to get back to first client.
    while (micros() < startTime)  // Wait for startTime before beginning
        ;

    if (LOCAL_SAMPLING) setupTimer(getSample);
    digitalWrite(READY_PIN, HIGH);  // Indicate that main loop is starting.
}

/*
 * Main loop. Call writeData() for serial output, switch to next client channel
 * (if ready), and constantly send sample requests to client.
 */
void loop() {
    int ready = queueLeastElements(samples, TOTAL_DEVICES);
    if (ready > 8) ready = 8;
    for (int i = 0; i < ready; i++) {
        if (LABVIEW_OUTPUT) {
            writeDataLabview();
        } else {
            writeDataNormal();
        }
    }

    // Switch to next device once current queue is sufficiently filled.
    const uint16_t current = queueElements(&samples[channel]) / 24;
    if (current > PACKET_COUNT / 2) nextChannel();

    // Request contains channel so client can be sure that the request was
    // intended for it as well as packetId so client can tell whether to
    // re-send the last packet (due to wireless issues) or send a new one.
    const char requestPacket[2] = {channel, packetId[channel]};
    RFduinoGZLL.sendToHost(requestPacket, sizeof(requestPacket));
}

/*
 * Called by hardware timer every READ_MICROS. Take a reading from analog pin
 * and append to queue.
 */
void getSample() {
    noInterrupts();
    bool success = queueAppend(&samples[CLIENTS], analogRead(2));
    interrupts();
    // Light up error LED if queue is full (due to wireless issues, probably).
    if (!success) digitalWrite(ERROR_PIN, HIGH);
    NRF_TIMER1->EVENTS_COMPARE[0] = 0;
}

/*
 * Write the next slice of data to serial output -- call this constantly in
 * order to keep up.
 * This is the LabView version.
 */
void writeDataLabview() {
    Serial.write('$');
    for (int i = 0; i < TOTAL_DEVICES; i++) {
        const uint_fast16_t value = queuePop(&samples[i]);
        Serial.write(lowByte(value));
        Serial.write(highByte(value));
    }

    for (int i = 0; i < 8 - CLIENTS; i++) {
        Serial.write(lowByte(0));
        Serial.write(highByte(0));
    }
    Serial.println("");
}

void writeDataNormal() {
    for (int i = 0; i < TOTAL_DEVICES; i++) {
        const uint_fast16_t value = queuePop(&samples[i]);
        Serial.print(value);
        Serial.print(" ");
    }
    Serial.println("");
}

/*
 * Called on receiving a wireless packet. Set nextChannel to true if ready
 * to move on to next client. Also --
 * During setup: set clientAcknowledged to true if client replied.
 * During sampling/main loop: call handleNewSamples().
 */
void RFduinoGZLL_onReceive(device_t device, int rssi, char *data, int len) {
    // Each "client" device is actually a GZLL host, so the device_t device
    // parameter doesn't actually identify it. Thus, data[0] contains an ID.
    const uint8_t currentDevice = data[0];

    // Don't proccess data from previous device after moving on to the next.
    if (len == 0 || currentDevice != channel) return;

    if (len == 32 && packetId[currentDevice] == data[1]) {
        handleNewSamples(currentDevice, data);
    } else if (len == 2 && data[1] == 'p') {
        pingAcknowledged = true;
    } else if (len == 2 && data[1] == 't') {
        timeAcknowledged = true;
    }
}

/*
 * RFduinoGZLL_onReceive() helper for sensor readings -- copy samples into
 * corresponding samples[] queue.
 */
void handleNewSamples(const uint8_t device, const char *data) {
    noInterrupts();
    const uint_fast16_t currentSamples = queueElements(&samples[device]);
    // Copy received data into queue starting at position.
    if (currentSamples / SAMPLE_COUNT < PACKET_COUNT) {
        packetId[device]++;
        const unsigned int position = samples[device].head / SAMPLE_COUNT;
        memcpy((void *)&samples[device].pkts[position], data,
                sizeof(samples[device].pkts[position]));
        samples[device].head = (samples[device].head + SAMPLE_COUNT) % QUEUE_SIZE;
    } else {
        // Queue for device is full -- this drops data/throws sampling out of sync.
        digitalWrite(ERROR_PIN, HIGH);
    }
    interrupts();
}

/*
 * Switch to wireless channel corresponding with next client.
 */
void nextChannel() {
    channel = (channel + 1) % CLIENTS;
    switchChannel(channel);
}
