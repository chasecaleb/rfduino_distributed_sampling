#include <Arduino.h>
#include <RFduinoGZLL.h>
#include <Wire.h>
#include "strainGauge.h"
#include "config.h"

void strainGaugeCalibrate(int target) {
    bool calibrateSuccess;
    // Calibration doesn't always behave...
    do {
        calibrateSuccess = attemptCalibration(target);
        if (!calibrateSuccess) digitalWrite(ERROR_PIN, HIGH);
    } while (!calibrateSuccess);
    digitalWrite(ERROR_PIN, LOW);
}

bool attemptCalibration(int target) {
    Wire.beginOnPins(5,6);
    const int MARGIN = 10;
    const int DELAY_TIME = 200;

    int set = 512;
    for  (int i = 8; i >= 0; i--) {
        if (readGauge(10) > target) {
            set += pow(2, i);
        } else {
            set -= pow(2, i);
        }
        writeValue(set);
        delay(DELAY_TIME);
    }
    return abs(target - readGauge(10)) < MARGIN;
}

int readGauge(int avgTimes) {
    int read = 0;
    for (int i = 0; i < avgTimes; i++) {
        read += analogRead(2) / avgTimes;
        delay(5);
    }
    return read;
}

void writeValue(int resistance) {
    int value = 1024 + resistance;
    int enable_rdac = 0x1C03;

    Wire.beginTransmission(46);

    Wire.write(enable_rdac >> 8);
    Wire.write(enable_rdac & 0xFF);

    Wire.write(value >> 8);
    Wire.write(value & 0xFF);
    Wire.endTransmission();
}

void blinkLed(int pin, int times) {
    for (int i = 0; i < times; i++) { // Must be even to turn back off.
        digitalWrite(pin, HIGH);
        delay(250);
        digitalWrite(pin, LOW);
        delay(250);
    }
}
