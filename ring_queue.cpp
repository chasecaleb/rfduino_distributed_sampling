#include <stdint.h>
#include <string.h>
#include <Arduino.h>
#include "ring_queue.h"

uint_fast16_t queueElements(Queue *queue) {
    // Double mod'ing like this wraps negative values back around.
    // I.E. (1 - 2) % 10 = -1, which is 65535 as an uint16_t. This way it
    // would return 9 as expected instead.
 
    // noInterrupts();
    // This could be negative, so it MUST be signed. Or else.
    int_fast16_t difference = queue->head - queue->tail;
    // interrupts();
    return ((difference % QUEUE_SIZE) + QUEUE_SIZE) % QUEUE_SIZE;
}

uint_fast16_t queueLeastElements(Queue *queues, int numQueues) {
    uint_fast16_t least = queueElements(&queues[0]);
    for (int i = 1; i < numQueues; i++) {
        int x = queueElements(&queues[i]);
        if (x < least) least = x;
    }
    return least;
}

bool queueAppend(Queue *queue, const uint_fast16_t value) {
    // Queue is full, no room to append.
    if (queueElements(queue) == QUEUE_SIZE - 1) return false;

    // noInterrupts();
    volatile Packet *pkt = &queue->pkts[queue->head / SAMPLE_COUNT % PACKET_COUNT];
    const unsigned int pktHead = (queue->head % SAMPLE_COUNT) * SAMPLE_SIZE / 8;
    const unsigned int offset = (queue->head * SAMPLE_SIZE) % 8;

    // Clear destination bits
    const uint16_t maskLow = (1 << (8-offset)) - 1;
    const uint16_t maskHigh = (1 << (2+offset)) - 1;
    pkt->data[pktHead] &= ~maskLow;
    pkt->data[pktHead+1] &= ~(maskHigh << (6 - offset));

    // Store value to array
    pkt->data[pktHead] |= value >> (offset + 2);
    pkt->data[pktHead+1] |= (value << (6 - offset)) & 0xFF;

    queue->head = (queue->head + 1) % QUEUE_SIZE;
    // interrupts();
    return true;
}

uint_fast16_t queuePop(Queue *queue) {
    // Queue is empty but this was still called, so return pseudo-error value.
    if (queueElements(queue) == 0) return 65535;

    // noInterrupts();
    volatile Packet *pkt = &queue->pkts[queue->tail / SAMPLE_COUNT % PACKET_COUNT];
    const unsigned int pktTail = (queue->tail % SAMPLE_COUNT) * SAMPLE_SIZE / 8;
    const unsigned int offset = (queue->tail * SAMPLE_SIZE) % 8;

    uint_fast16_t value = (pkt->data[pktTail] << (offset + 2)) & 0x3FF;
    value |= pkt->data[pktTail+1] >> (6 - offset);

    queue->tail = (queue->tail + 1) % QUEUE_SIZE;
    // interrupts();
    return value;
}
