#include <stdint.h>
#include <Arduino.h>
#include <RFduinoGZLL.h>
#include "shared.h"
#include "config.h"

/*
 * Configure/start hardware timer 1 to call getSample.
 * This allows the host to handle a heavy load outputting/processing data from
 * clients and still record its own data with high precision.
 * Google "timer interrupt callbacks" if that doesn't make sense.
 *
 * REFERENCES:
 * http://forum.rfduino.com/index.php?topic=155.5
 * https://github.com/finnurtorfa/nrf51/blob/master/lib/nrf51sdk/Nordic/nrf51822/Board/nrf6310/timer_example/main.c
 */
void setupTimer(callback_t callback)
{
    // WARNING: most of this is magic/should not be touched.
    NRF_TIMER1->TASKS_STOP = 1;  // Stop timer
    NRF_TIMER1->MODE = TIMER_MODE_MODE_Timer;
    NRF_TIMER1->BITMODE = (TIMER_BITMODE_BITMODE_16Bit << TIMER_BITMODE_BITMODE_Pos);
    NRF_TIMER1->PRESCALER = 4;  // SysClk/2^PRESCALER) =  16,000,000/16 = 1us resolution
    NRF_TIMER1->TASKS_CLEAR = 1;  // Clear timer
    NRF_TIMER1->CC[0] = 1000000/READ_HZ;  // Must be <= 65535 (16 bits)
    NRF_TIMER1->INTENSET = TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos;
    NRF_TIMER1->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);
    attachInterrupt(TIMER1_IRQn, callback);  // Call getSample when timer up.
    NRF_TIMER1->TASKS_START = 1;  // Start TIMER
}

void switchChannel(uint8_t channel) {
    nrf_gzll_disable();
    uint8_t value = CHANNEL_OFFSET + channel;
    uint8_t channelTable[1] = {value};

    while (nrf_gzll_is_enabled())
        ;

    nrf_gzll_set_channel_table(channelTable, 1);
    nrf_gzll_enable();
}
